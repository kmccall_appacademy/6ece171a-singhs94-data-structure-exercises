# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  return arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  if arr == arr.sort
    return true
  else
    return false
  end
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  i = 0
  count = 0
  vowel = ['a','e','i','o','u']
  array = str.downcase.split('')
  while i < array.length
    if vowel.include?(array[i])
      count += 1
    end
    i = i + 1
  end
  return count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  return str.delete('aeiouAEIOU')
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  array = []
  int = int.to_s
  array = int.split('')
  return array.sort {|x,y| y <=> x}
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  array = str.downcase.split('')
  array2 = str.downcase.split('')
  if array.uniq != array2
    return true
  else
    return false
  end
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  result = "(#{arr[0].to_s + arr[1].to_s+arr[2].to_s}) #{arr[3].to_s + arr[4].to_s + arr[5].to_s}-#{arr[6].to_s + arr[7].to_s + arr[8].to_s + arr[9].to_s}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  array = str.split(',')
  array.max.to_i - array.min.to_i

end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  # your code goes here
  if offset > arr.length
    offset = offset%arr.length
  elsif offset < 0
    shift_to_front = arr.drop(arr.length - offset.abs)
    shift_to_end = arr.take(arr.length - offset.abs)
    return shift_to_front + shift_to_end
  end
  shift_to_end = arr.take(offset)
  shift_to_front = arr.drop(offset)
  shift_to_front + shift_to_end
end
